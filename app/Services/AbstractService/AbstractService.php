<?php


namespace App\Services\AbstractService;


use App\Services\AbstractService\Contracts\AbstractServiceInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AbstractService
 * @package App\Services\AbstractService
 */
abstract class AbstractService implements AbstractServiceInterface
{

}
