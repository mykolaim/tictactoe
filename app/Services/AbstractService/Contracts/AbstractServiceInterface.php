<?php


namespace App\Services\AbstractService\Contracts;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Interface AbstractServiceInterface
 * @package App\Services\AbstractService\Contracts
 */
interface AbstractServiceInterface
{
    /**
     * @return Collection
     */
    public function getAll(): Collection;


    /**
     * @param array $data
     * @return Model
     */
    public function store(array $data): Model;


    /**
     * @param Model $model
     * @param array $data
     * @return Model
     */
    public function update(Model $model, array $data): Model;


    /**
     * @param Model $model
     * @return bool
     */
    public function delete(Model $model): bool ;
}
