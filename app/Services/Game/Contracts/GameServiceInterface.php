<?php


namespace App\Services\Game\Contracts;


use App\Services\AbstractService\Contracts\AbstractServiceInterface;

/**
 * Interface GameServiceInterface
 * @package App\Services\Game\Contracts
 */
interface GameServiceInterface extends AbstractServiceInterface
{

}
