<?php


namespace App\Services\Game;


use App\Models\Game\Game;
use App\Models\Game\Logic;
use App\Services\Game\Contracts\LogicServiceInterface;

/**
 * Class LogicService
 * @package App\Services\Game
 */
class LogicService implements LogicServiceInterface
{
    /**
     * @param string $board
     * @return array
     */
    public function handle(string $board): array
    {
        $board = $this->toArray($board);
        //check that status isn't finished
        //if it's good - make move
        return ['status' => $this->verifyStatus($board), 'board' => $board];
    }

    /**
     * @param string $board
     * @return string
     */
    protected function verifyStatus(string $board): string
    {
        $board = $this->toArray($board);
        return Game::X_WON;
    }

    /**
     * @param string $board
     * @return array
     */
    protected function toArray(string $board): array
    {
        return str_split($board, Logic::WIDTH);
    }

    /**
     * @param array $board
     * @return string
     */
    protected function toString(array $board): string
    {
        return implode('', $board);
    }
}
