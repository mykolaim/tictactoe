<?php


namespace App\Services\Game;


use App\Models\Game\Game;
use App\Services\AbstractService\AbstractService;
use App\Services\Game\Contracts\GameServiceInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class GameService
 * @package App\Services\Game
 */
class GameService extends LogicService implements GameServiceInterface
{
    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return Game::query()->get();
    }


    /**
     * @param array $data
     * @return Model
     */
    public function store(array $data): Model
    {
        $data = array_merge($data, $this->handle($data['status']));
        return Game::query()->create($data);
    }

    /**
     * @param Model $model
     * @param array $data
     * @return Model
     */
    public function update(Model $model, array $data): Model
    {
        $data = array_merge($data, $this->handle($data['status']));
        $model->update($data);
        return $model->refresh();
    }

    /**
     * @param Model $model
     * @return bool
     * @throws \Exception
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }
}
