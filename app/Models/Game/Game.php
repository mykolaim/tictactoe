<?php


namespace App\Models\Game;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Game
 * @property int $id
 * @property string $board
 * @property string $status
 * @package App\Models
 */
class Game extends Model
{

    /**
     * Game statuses
     */
    public const RUNNING = 'running';
    public const X_WON = 'x_won';
    public const O_WON = 'o_won';
    public const DRAW = 'draw';
    public const STATUSES = [
        self::RUNNING,
        self::X_WON,
        self::O_WON,
        self::DRAW
    ];



    /**
     * @var array
     */
    public $fillable = ['board', 'status'];

}
