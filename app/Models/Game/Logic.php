<?php


namespace App\Models\Game;


/**
 * Class Logic
 * @package App\Models\Game
 */
class Logic
{
    /**
     * Game values
     */
    public const X = 'X';
    public const O = 'O';
    public const EMPTY = '-';
    public const SYMBOLS = [
        self::EMPTY,
        self::X,
        self::O
    ];


    public const WIDTH = 3;
    public const HEIGHT = 3;
    public const STRING_LENGTH = self::WIDTH * self::HEIGHT;

    /**
     * @return string
     */
    public static function getPlayerSymbol(): string
    {
        return self::X;
    }

    /**
     * @return string
     */
    public static function getBotSymbol(): string
    {
        return self::O;
    }
}
