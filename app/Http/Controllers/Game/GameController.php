<?php

namespace App\Http\Controllers\Game;

use App\Http\Controllers\Controller;
use App\Http\Requests\Game\StoreRequest;
use App\Http\Requests\Game\UpdateRequest;
use App\Http\Resources\Game\GameResource;
use App\Models\Game\Game;
use App\Services\Game\Contracts\GameServiceInterface;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

/**
 * Class GameController
 * @package App\Http\Controllers
 */
class GameController extends Controller
{
    /**
     * @var GameServiceInterface
     */
    protected $gameService;

    /**
     * GameController constructor.
     * @param GameServiceInterface $gameService
     */
    public function __construct(GameServiceInterface $gameService)
    {
        $this->gameService = $gameService;
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return GameResource::collection($this->gameService->getAll());
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $game = $this->gameService->store($request->only(['board']));
        return response()->json(
            ['location' => route('game-update', ['game' => $game->id])],
            Response::HTTP_CREATED
        );
    }

    /**
     * @param Game $game
     * @param UpdateRequest $request
     * @return GameResource
     */
    public function update(Game $game, UpdateRequest $request)
    {
        return GameResource::make($this->gameService->update($game, $request->only(['board'])));
    }

    /**
     * @param Game $game
     * @return Response
     */
    public function delete(Game $game): Response
    {
        $this->gameService->delete($game);
        return response(null, Response::HTTP_OK);
    }
}
