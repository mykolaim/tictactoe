<?php


namespace App\Http\Resources\Game;


use App\Models\Game\Game;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class GameResource
 * @package App\Http\Resources\Game
 * @mixin Game
 */
class GameResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'     => $this->id,
            'status' => $this->status,
            'board'  => $this->board
        ];
    }
}
