<?php


namespace App\Http\Requests\Game;


use App\Models\Game\Game;
use App\Models\Game\Logic;
use App\Rules\Game\AllowedSymbols;
use Urameshibr\Requests\FormRequest;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\Game
 */
class UpdateRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        /** @var Game $game */
        $game = Game::query()->findOrFail($this->route('game'));
        return $game->status === Game::RUNNING;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $length = Logic::STRING_LENGTH;
        return [
            'board' => ['required', "between:$length,$length", new AllowedSymbols()]
        ];
    }

}
