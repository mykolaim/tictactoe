<?php


namespace App\Http\Requests\Game;


use App\Models\Game\Logic;
use App\Rules\Game\AllowedSymbols;
use Urameshibr\Requests\FormRequest;

/**
 * Class StoreRequest
 * @package App\Http\Requests\Game
 */
class StoreRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $length = Logic::STRING_LENGTH;
        return [
            'board' => ['required', "between:$length,$length", new AllowedSymbols()]
        ];
    }

}
