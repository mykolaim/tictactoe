<?php

namespace App\Providers;

use App\Services\Game\Contracts\GameServiceInterface;
use App\Services\Game\GameService;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(GameServiceInterface::class, GameService::class);
    }
}
