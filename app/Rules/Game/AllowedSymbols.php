<?php


namespace App\Rules\Game;


use App\Models\Game\Logic;
use Illuminate\Validation\Rule;

class AllowedSymbols extends Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !preg_match('/[^'.implode('',Logic::SYMBOLS).']/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute can contain only next symbols:'.implode(' ',Logic::SYMBOLS);
    }

}
