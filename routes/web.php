<?php

/** @var \Laravel\Lumen\Routing\Router $router */


$router->get('/', function () use ($router) {
    return 'Welcome to the Tic Tac Toe game!';
});

$router->group(['prefix' => 'games', 'namespace' => 'Game'], function () use ($router) {
    $router->get('/', 'GameController@index');
    $router->post('/', 'GameController@store');
    $router->put('{game}', ['action' => 'GameController@update', 'as' => 'game-update']);
    $router->delete('{game}', 'GameController@delete');
});
